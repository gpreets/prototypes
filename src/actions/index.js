import * as types from './actionTypes';

export const selectCharacter = (payload) => { 
    return (dispatch, getState) => {
        if(!payload) {
            return dispatch({
                type: 'DEFAULT'
            });
        }

        dispatch({
            type: types.CHARACTER_SELECTED,
            payload: [{loading: 'loading ...'}]
        });

        fetch(payload)
            .then(res => res.json())
            .then(res => res.films)
            .then(films => {
                if(!films) {
                    throw Error(`While fetching data from ${payload}`);
                }
                if(!films.length) {
                    throw Error(`No films found for the selected char at url ${payload}`);
                }
                return fetchTitles(films)
            })
            .then(res => {            
                return dispatch(fetchSuccess(res));
            })
            .catch(error => {
                console.error(error);
                return dispatch(fetchError(error));
            });
    };
};

const fetchSuccess = (res) => {
    return {
        type: types.CHARACTER_SELECTED,
        payload: res
    };
};

const fetchError = (error) => {
    return {
        type: 'ERROR',
        payload: [{error: error.toString()}]
    };
};

const fetchTitles = async (films) => {
    const titles = await films.map(film => {
        return fetch(film)
            .then(res => res.json())
            .then(({ 
                episode_id,
                title, 
                director, 
                producer, 
                release_date
            }) => ({
                episode_id,
                title, 
                director, 
                producer, 
                release_date: new Date(release_date).toDateString()
            }));
    });

    return Promise.all(titles)
        .then(res => res.sort((a,b) => new Date(b.release_date).getFullYear() - new Date(a.release_date).getFullYear()));
};