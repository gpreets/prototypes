import { combineReducers } from 'redux';
import initialState from './initialState';

const charactersReducer = () => initialState.characters;
const titlesReducer = (state, action) => {
    switch (action.type) {
        case 'CHARACTER_SELECTED':
            return action.payload;
        case 'ERROR':
            return action.payload;
        default:
            return [{message: 'Select a character above to see a list of movies.'}]
    }
};

const rootReducer = combineReducers({
    characters: charactersReducer,
    titles: titlesReducer
})

export default rootReducer