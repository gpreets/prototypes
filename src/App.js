import React, { Component } from 'react';
import {connect} from 'react-redux';
import './App.css';
import { selectCharacter } from './actions';

class App extends Component {
  constructor(props) {
    super(props);
    this.charRef = React.createRef();
  }

  render() {
    const {
      characters = [],
      titles,
      selectCharacter
    } = this.props;
    const charactersList = characters.map(char => (<option key={char.url} value={char.url}>{char.name}</option>));
    
    return (
      <div className="container-fluid text-center">
        <header className="main-header">
          <img className="sw-logo" alt="star wars" src="https://static-mh.content.disney.io/starwars/assets/navigation/sw_logo_stacked@2x-52b4f6d33087.png" />
          <h2 className="main-header__title">Select a character from drop-down below to display the associated list of movies.</h2>
          <form>
            <select onChange={() => selectCharacter(this.charRef.current.value)}  ref={this.charRef} className="custom-select custom-select-lg mb-3">
              <option key="default" value="">Select a character</option>
              {charactersList}
            </select>
          </form>
        </header>
        <section>
          <ul className="list-group">
            {titles && titles.map((film, index) => {
              return (
                <React.Fragment key={`fragment-${film.episode_id}`}>
                  {(film.message && index === 0) && <span className="default-message">{film.message}</span>}
                  {(film.loading && index === 0) && <span className="loading-message">{film.loading}</span>}
                  {film.error && <span className="error-message">{film.error}</span>}
                  {film.title && 
                      <li className="list-group-item" key={film.episode_id}>
                        <h2>{film.title}</h2>
                        {film.director && <h3 key={`director-${film.episode_id}`}><span>Director:</span>{film.director}</h3>}
                        {film.producer && <h3 key={`producer-${film.episode_id}`}><span>Producer(s):</span>{film.producer}</h3>}
                        {film.release_date && <h3 key={`date-${film.episode_id}`}><span>Released on:</span>{film.release_date}</h3>}
                      </li>
                  }
                </React.Fragment>
              );
            })}          
          </ul>
        </section>
      </div>
    );
  }
}

const mapStateToProps = ({characters, titles}) => ({
    characters,
    titles
});

const mapDispatchToProps = {
    selectCharacter
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
